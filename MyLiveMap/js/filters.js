'use strict';

/* Filters */

angular.module('threatPortal.filters')

.filter('interpolate', ['version', function(version) {
    return function(text) {
        return String(text).replace(/\%VERSION\%/mg, version);
    };
}])

.filter('toNumber', function() {
        return function(input) {
            return parseInt(input, 10);
        };
    })
    .filter('toDate', function() {
        return function(date) {
            return new Date(date);
        };
    }).filter('toCountryName', ['CountryCodeService', function(CountryCodeService) {
        return function(a2CountryCode) {
            return a2CountryCode ? CountryCodeService.getByA2Code(a2CountryCode).name : "null";
        };
    }])

.filter('threatWikiProductName', function() {
    return function(productName) {
        if (productName == "AB&AV") {
            return "Anti-Bot & Anti-Virus";
        }
        return productName;
    };
})

.filter('ellipsisFilter', function() {
    /*	Shortens the passed string to params.maxLength characters,
     *	stops on a space char before maxLength if params.wholeWords is set to TRUE. */
    return function(text, params) {
        if (params.maxLength === undefined) {
            params.maxLength = 0;
        }
        if (text.length > params.maxLength) {
            var endIndex = params.maxLength;
            var lastSpace = text.substring(0, endIndex).lastIndexOf(" ");
            if (params.wholeWords && (lastSpace > 0)) {
                endIndex = lastSpace;
            }
            return text.substring(0, endIndex) + "...";
        } else {
            return text;
        }
    };
})

.filter('stripHTML', function() {
    /* Removes all HTML <elements> from the text. This won't remove special encoding
     * like &nbsp; so use with [ng-bind-html] attributes to parse these if they exist. */
    return function(text) {
        return String(text).replace(/<[^>]+>/gm, '');
    };
})

.filter('joinBy', function() {
    return function(inputArr, delimiter) {
        //		var deleteme = testfunc(inputArr);
        return (inputArr || []).join(delimiter || ', ');
    };
})

.filter('addSearchLinks', function() {
    return function(inputArr) {
        if (Object.prototype.toString.call(inputArr) !== '[object Array]') {
            // If the input isn't an array, treat it as a primitive
            return '<a href="./#/search/%22' + inputArr + '%22">' + inputArr + '</a>';
        } else {
            // The object is an array, loop over all the values and return an array of strings
            var result = [];
            var length = inputArr.length;
            for (var i = 0; i < length; i++) {
                // Create a link to exact-match-search for each "VALUE", and push it to the output array
                result.push('<a href="./#/search/%22' + inputArr[i] + '%22">' + inputArr[i] + '</a>');
            }
            return result;
        }
    };
});
