(function() {
    'use strict';

    angular.module('threatPortal.controllers').controller('StartupCtrl', ['$scope', '$http', 'emulationModalService', '$rootScope', 'blogsService', 'ServerServices', homeController]);

    function homeController($scope, $http, emulationModalService, $rootScope, blogsService, ServerServices) {


        $scope.openModal = function() {
            var modalInstance = emulationModalService.openEmulationModal();
            sendEmulationEvent(1);
        };

        $rootScope.query = '';

        $scope.url = 'rest/threatPortal/startup';
        $scope.loadingStats = true;
        $scope.loadingBlogs = true;
        $scope.topFamiliesChart = {};

        ServerServices.Home.GetStartup()
            .success(function(data) {
                var colorsArray = ['#5EBDF1', '#8CCFF3', '#43ACE6', '#3CA3DC', '#2689C0'];
                var i = 0;
                var statesArray = [
                    ["Family", "Number", {
                        role: 'style'
                    }]
                ];
                angular.forEach(data.searchResult, function(value, key) {
                    var stateitem = [value.name, parseFloat(value.count), 'fill-color:' + colorsArray[i] + ';stroke-width: 0'];
                    statesArray.push(stateitem);
                    i++;
                });

                $scope.topFamiliesChart.data = statesArray;
                $scope.topFamiliesChart.type = 'BarChart';
                $scope.topFamiliesChart.options = {
                    vAxis: {
                        gridlines: {
                            color: 'transparent'
                        },
                        textStyle: {
                            color: '#5B759A',
                            fontSize: 12,
                            fontName: 'Exo-2'
                        }
                    },
                    hAxis: {
                        gridlines: {
                            color: 'transparent'
                        },
                        baselineColor: 'transparent'
                    },
                    enableInteractivity: 'false',
                    backgroundColor: {
                        fill: 'transparent'
                    },
                    chartArea: {
                        left: 70,
                        top: 0,
                        height: '90%',
                        width: '100%'
                    },
                    height: 110
                };


                $scope.loadingStats = false;
            })
            .error(function(data, status) {});

        $scope.blogs = {};
        blogsService.getBlogsFromXml().then(function(data) {
            $scope.blogs = data;
            $scope.loadingBlogs = false;
        });
    }
})(angular);
