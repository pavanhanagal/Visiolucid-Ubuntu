(function(angular) {

    angular.module('threatPortal.services').service('mapEventsService', ['$rootScope', '$interval', function($rootScope, $interval) {
        var onAttackCallBacks = [];
        var onCountryClickedCallBacks = [];
        var attacks = [];
        var forbiddenCountries = ["hk", "sg"];
        return {
            addAttack: addAttack,
            countryClicked: countryClicked,
            onCountryClicked: onCountryClicked,
            checkForAttacks: checkForAttacks
        };

        function checkForAttacks() {
            if (attacks.length) {
                var attack = attacks.splice(attacks.length - 1, 1)[0];
                $rootScope.$broadcast("attack", attack);

            }
        }


        function addAttack(attackName, sourcecountry,sourcestate,sourcelongitude,sourcelatitude,destinationcountry,destinationstate,destinationlongitude,destinationlatitude,type) {
            if (_.find(forbiddenCountries, function(x) {
                    return sourcecountry.toLowerCase() == x || destinationcountry.toLowerCase() == x;
                })) {
                return;
            }
            attacks.push({
                attack: attackName,
                sourcecountry: sourcecountry,
                sourcestate:sourcestate,
                sourcelongitude:sourcelongitude,
                sourcelatitude:sourcelatitude,
                destinationcountry: destinationcountry,
                destinationstate: destinationstate,
                destinationlongitude:destinationlongitude,
                destinationlatitude:destinationlatitude,
                type:type
            });
        }

        function countryClicked(countryId, mouseX, mouseY) {
        	var length = onCountryClickedCallBacks.length;
            for (var i = length- 1; i >= 0; i--) {
                onCountryClickedCallBacks[i]({
                    countryId: countryId,
                    mouseX: mouseX,
                    mouseY: mouseY
                });
            };
        }

        function onCountryClicked(callback) {
            onCountryClickedCallBacks.push(callback);

        }

    }]);
})(angular);
