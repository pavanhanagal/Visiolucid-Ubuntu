(function(angular) {
    var app = angular.module('threatPortal.directives');

    app.directive('externalLinks', ['mapEventsService', function(mapEventsService) {

        // Runs during compile
        return {
            restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
            templateUrl: 'partials/live-map/directives/externalLinks.html',
            replace: true
        };
    }]);
})(angular)
