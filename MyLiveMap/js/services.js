'use strict';
(function(angular) {


    /* Services */

    angular.module('threatPortal.services')
        .service('blogsService', ['$http', '$q', function($http, $q) {
            var blogsCache = null;
            return {
                getBlogsFromXml: function() {
                    var deffered = $q.defer();
                    if (blogsCache) {
                        deffered.resolve(blogsCache);
                    } else {
                        $http.get('blog.xml', {
                            transformResponse: function(data) {
                                var x2js = new X2JS();
                                var json = x2js.xml_str2json(data);
                                return json.rss.channel.item;
                            }
                        }).success(function(result) {

                            blogsCache = result;
                            deffered.resolve(blogsCache);
                        });
                    }

                    return deffered.promise;
                }
            };
        }]).service('searchService', ['$http', '$q', function($http, $q) {
            var searchUrl = 'rest/threatPortal/search';
            var mapUrl = 'rest/threatPortal/map';

            var resultsCache = {
                malwares: {},
                advisories: {}
            };

            return {
                getAdvisory: function(id) {
                    var defferd = $q.defer();
                    if (resultsCache.advisories[id]) {
                        defferd.resolve(resultsCache.advisories[id]);
                    } else {
                        getAdvisoryFromServer(id).success(function(data) {
                            if (!data.advisories.searchResult) {
                                defferd.reject();

                            } else {
                                saveSearchResult(data);
                                defferd.resolve(data.advisories.searchResult[0]);
                            }

                        })
                    }


                    return defferd.promise;
                },
                getMalware: function(id) {
                    var defferd = $q.defer();
                    if (resultsCache.malwares[id]) {
                        defferd.resolve(resultsCache.malwares[id]);
                    } else {
                        getMalwareFromServer(id).success(function(data) {
                            if (!data.malware.searchResult) {
                                defferd.reject();

                            } else {
                                saveSearchResult(data);
                                defferd.resolve(data.malware.searchResult[0]);
                            }

                        })
                    }

                    return defferd.promise;
                },
                saveSearchResult: saveSearchResult,
                doSearch: doSearch,
                getMap: getMap
            };

            function getMap(family, product) {
                return $http({
                    method: 'GET',
                    url: mapUrl,
                    params: {
                        'malwareFamily': family,
                        'malwareProduct': product
                    }
                });
            }

            function saveSearchResult(result) {
                if (result.advisories && result.advisories.searchResult) {
                    result.advisories.searchResult.forEach(function(x) {
                        resultsCache.advisories[x.id] = x;
                        resultsCache.advisories[x.id].queryTime = result.advisories.queryTime;
                    });
                }
                if (result.malware && result.malware.searchResult) {
                    result.malware.searchResult.forEach(function(x) {
                        resultsCache.malwares[x.malwareId] = x;
                        resultsCache.malwares[x.malwareId].queryTime = result.malware.queryTime;
                    });
                }

            }

            function doSearch(query, tab, page) {
                return $http({
                    method: 'GET',
                    url: searchUrl,
                    params: {
                        'q': query,
                        'p': page,
                        't': tab
                    }
                });
            }

            function getMalwareFromServer(id) {
                return doSearch(id, "malware", null);
            }

            function getAdvisoryFromServer(id) {
                return doSearch(id, "advisories", null);
            }
        }])


    .service('emulationService', function() {

            var emulationReport = [];

            return {
                getActivityFilters: function() {
                    return emulationReport;
                },
                setFamilyFilters: function(json) {
                    emulationReport = json;
                }
            };
        })
        .service('emulationModalService', ['$modal', function($modal) {
            return {
                openEmulationModal: function() {
                    return $modal.open({
                        templateUrl: 'partials/home/emulationModal.html',
                        controller: function($scope, $modalInstance) {
                            $scope.close = function() {
                                $modalInstance.dismiss('cancel');
                            };
                        },
                        keyboard: false,
                        backdrop: 'static'
                    });
                }
            };
        }]);

})(angular);
