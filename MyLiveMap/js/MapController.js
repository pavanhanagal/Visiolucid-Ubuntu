(function() {
    'use strict';
    angular.module('threatPortal.controllers').controller('MapCtrl', ['$scope', 'atmosphereService', '$location', 'mapEventsService', '$modal',
        'CountryCodeService', '$timeout', '$rootScope', 'serverService', '$interval', 'consts', MapCtrl
    ]);

    function MapCtrl($scope, atmosphereService, $location, mapEventsService, $modal, CountryCodeService, $timeout, $rootScope, serverService, $interval, consts) {

        var existingInterval = $interval(function() {
            mapEventsService.checkForAttacks();
        }, parseInt(consts.checkForAttacks));

        $scope.$on('$destroy', function() {
            $interval.cancel(existingInterval);
        });

        /* GET STARTUP DATA - ATTACKS COUNT ETC */
        //serverService.getStartData().then(function(e) {
        //    $scope.todayAttacksCount = e.data.todayTotalAttacks;
        //    $scope.yesterdayAttacksCount = e.data.yesterdayTotalAttacks;
        //    $scope.topAttackers = e.data.topAttackingCountries;
        //    $scope.topTargets = e.data.topTargetCountries;
        //}, function(e) {});

        $.ajax("webserver/startup.php").done(function (data) {
            var attacksToday = atmosphere.util.parseJSON(data);
           // $scope.todayAttacksCount = attacksToday.todayTotalAttacks
           // $scope.yesterdayAttacksCount = attacksToday.yesterdayTotalAttacks;
            $scope.topAttackers = attacksToday.topAttackingCountries;
            $scope.topTargets = attacksToday.topTargetCountries;
        });

	// To load dynamically todayAttacksCount value once the attack occurs.
	
	var websocket = new WebSocket("ws:localhost:8089");
	websocket.onopen = function(e)
	{
		writeToScreen("You have have successfully connected to the server");
	}
	websocket.onmessage = function (e)
	{
		writeToScreen(e.data);
		var jsonArray = JSON.parse(e.data);	
		$scope.todayAttacksCount = jsonArray.vectors.length;
	}

        $scope.atmosphere = {
            transport: '',
            messages: []
        };

        init();

        var showSimulator = false;


        function init() {          

            $scope.atmosphere.socket = atmosphereService.subscribe(initRequest());
            
            function initRequest() {
                var request = {
                    url: '',
                    contentType: 'application/json',
                  //  logLevel: 'debug',
                    transport: '',
                    trackMessageLength: true,
                    reconnectInterval: 5000,
                    enableXDR: true,
                    timeout: 60000
                };

                request.onOpen = function(response) {
                    $scope.atmosphere.transport = response.transport;
                };

                request.onClientTimeout = function(response) {
                    $scope.atmosphere.content = 'Client closed the connection after a timeout. Reconnecting in ' + request.reconnectInterval;
                    setTimeout(function() {
                    	$scope.atmosphere.socket = atmosphereService.subscribe(request);
                    }, request.reconnectInterval);
                };

                request.onReopen = function(response) {
                };

                //request.onTransportFailure = function(errorMsg,request) {
                //    atmosphere.util.info(errorMsg);
                //    request.fallbackTransport = 'long-polling';
                //};

                request.onMessage = function(response) {
                    var responseText = response.responseBody;
                    try {
                        var message = atmosphere.util.parseJSON(responseText);
                        if (message.todayTotalAttacks) {
                            $scope.todayAttacksCount = message.todayTotalAttacks;
                        } else if(message.yesterdayTotalAttacks){
                        	 $scope.yesterdayAttacksCount = message.yesterdayTotalAttacks;
                        }
                        else {
                            mapEventsService.addAttack(message.attackname,message.sourcecountry,message.sourcestate,message.sourcelongitude,message.sourcelatitude,message.destinationcountry,message.destinationstate,message.destinationlongitude,message.destinationlatitude,message.type);
                        }

                    } catch (e) {
                        console.error("Error parsing JSON: ", responseText);
                        throw e;
                    }
                };

                request.onClose = function(response) {
                };

                request.onError = function(response) {
                };

                request.onReconnect = function(request, response) {
                };
                return request;
            }
        }


      
        /* SIMULATOR  */
        if (showSimulator) {

            //   Attacks SIMULATOR
            var countries = ["NO"];


            $interval(function() {
                var rand1 = countries[Math.floor(Math.random() * countries.length)];
                var rand2 = countries[Math.floor(Math.random() * countries.length)];
                // while (rand2 == rand1) {
                //     rand2 = countries[Math.floor(Math.random() * countries.length)];
                // }
                mapEventsService.addAttack("afdafa", rand1, rand2);

            }, 1000);
            /* END SIMULATOR  */
        }
    }
})(angular);
