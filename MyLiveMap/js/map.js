(function (angular) {
 
    var app = angular.module('threatPortal.directives');
    app.directive('map', ['$timeout', '$rootScope', 'mapEventsService', 'CountryCodeService', function($timeout, $rootScope, mapEventsService, CountryCodeService) {
        // Runs during compile
        return {
            // name: '',
            // priority: 1,
            // terminal: true,
            scope: {
                animationSpeed: '@',
                fadeOutSpeed: '@',
                borderColor: '@',
                arcThickness: '@'
            },
            // controller: function($scope, $element, $attrs, $transclude) {},
            // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
            restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
            // template: '',
           templateUrl: "partials/live-map/directives/map.html",
            replace: true,
            // transclude: true,
            // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
            link: function($scope, iElm, iAttrs, controller) {
                var map = null;
                var countryNamesDisplayed = [];
                $(window).bind('resize', paintMap);

                //timeout is required to paint elements properly
                $timeout(paintMap, 700);

                $rootScope.$on("attack", function(event, attack) {
                    var sourcecountry = CountryCodeService.getByA2Code(attack.sourcecountry).name;
                    var destinationcountry = CountryCodeService.getByA2Code(attack.destinationcountry).name;
                    addAttack(sourcecountry,attack.sourcestate,attack.sourcelongitude,attack.sourcelatitude,destinationcountry,attack.destinationstate,attack.destinationlongitude,attack.destinationlatitude,attack.type);
                });

                function paintMap() {
                    $("#map").html('');
                    init();
                }

                function init()
                {  
                    $scope.animationSpeed = window.isIE9 ? 0 : $scope.animationSpeed;
                    var destHeight = angular.element("#middleContainer").height() - 20;
                    var destWidth = angular.element("#middleContainer").width();
                    map = new Datamap({
                        element: document.getElementById('map'),
                        //   projection: 'mercator', //style of projection to be used. try "mercator"
                        geographyConfig: {
                            highlightOnHover: true,
                            popupOnHover: true,
                            popupTemplate: function (geography, data) { //this function should just return a string
                                return '<div class="countryHover"><strong>' + geography.properties.name + '</strong></div>';
                            },
                            borderColor: $scope.borderColor,
                            highlightFillColor: '#e45785',
                            borderWidth: 1
                        },
                        height: destHeight,
                        width: Math.min(destWidth, destHeight * 2),
                        fills: {
                            defaultFill: '#000'
                        },
                        done: function (datamap) {
                            datamap.svg.selectAll('.datamaps-subunit').on('click', function (geography, e) {
                                var countryData = CountryCodeService.getByA3Code(geography.id);
                                mapEventsService.countryClicked(countryData.iso2Code, d3.event.pageX, d3.event.pageY);
                            });
                        }
                    });

                    //For reading JSON file from local project location.
                    /* readTextFile("webserver/testAttackOne.json", function (text) {
                          var message = JSON.parse(text);						
                          console.log(message);						
                          mapEventsService.addAttack(message.attackname, message.sourcecountry, message.sourcestate, message.sourcelongitude, message.sourcelatitude, message.destinationcountry, message.destinationstate, message.destinationlongitude, message.destinationlatitude, message.type);
                        });   
                              
                     */

                    //For reading PHP file from local project location.
                    // $.ajax("webserver/MoreAttackVectors.php").done(function(data){		
                    //	var jsonArray = JSON.parse(data);
                    //	console.log(jsonArray.vectors);						
                    //	for (var i=0;i<jsonArray.vectors.length;i++)
                    //	{
                    //		 var message = jsonArray.vectors[i];
                    //		 mapEventsService.addAttack(message.attackname, message.sourcecountry, message.sourcestate, message.sourcelongitude, message.sourcelatitude, message.destinationcountry, message.destinationstate, message.destinationlongitude, message.destinationlatitude, message.type);
                    //	}                   
                    //});	
                    /* map.svg.call(d3.behavior.zoom().on("zoom", redraw));
	                   function redraw() {
	                     map.svg.selectAll("g").attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
	                 }*/

                    var websocket = new WebSocket("ws:localhost:8089");
                    websocket.onopen = function(e)
                    {
                        writeToScreen("You have have successfully connected to the server");                      
                    }

                    websocket.onmessage = function (e) {
                        writeToScreen(e.data);
						var jsonArray = JSON.parse(e.data);
						for (var i=0;i<jsonArray.vectors.length;i++)
                    	{							
							var message = jsonArray.vectors[i];
							mapEventsService.addAttack(message.attackname, message.sourcecountry, message.sourcestate, message.sourcelongitude, message.sourcelatitude, message.destinationcountry, message.destinationstate, message.destinationlongitude, message.destinationlatitude, message.type);
						}
                       
                    }
                }
               
				//For reading JSON file from local project location.
                function readTextFile(file, callback) {
                    var rawFile = new XMLHttpRequest();
                    rawFile.overrideMimeType("application/json");
                    rawFile.open("GET", file, true);
                    rawFile.onreadystatechange = function () {
                        if (rawFile.readyState === 4 && rawFile.status == "200") {
                            callback(rawFile.responseText);
                        }
                    }
                    rawFile.send(null);
                }
                
                function addAttack(originName,originalState,originLongitude,originLatitude,targetName,targetState,targetLongitude,targetLatitude,type) {
                    if (!map)
                        return;
                    var oCenter = latLngToXY(originLatitude,originLongitude);  //getCentroid(originId);
                    var originXY = oCenter;
                    var tCenter = latLngToXY(targetLatitude,targetLongitude); //getCentroid(targetId);
                    var destXY = tCenter;
                    var arcSharpness = 3 * Math.random();

                    // decide on arc color
                    var arcColor =	hexToRgb(getArcColor(type));

                    if ((oCenter !== null) && (tCenter !== null)) {

                        var squarePin = map.svg.append("svg:image")
                            .attr('x', originXY[0] - 11)
                            .attr('y', originXY[1] - 16)
                            .attr('width', 20)
                            .attr('height', 24)
                            .attr("xlink:href", "https://sc1.checkpoint.com/threatcloud/images/live-map/SquarePin.png");

                        showCountryName(originName,originalState, originXY[0] - 11, originXY[1] - 16, $scope.fadeOutSpeed / 2, $scope.animationSpeed);
                        var arc = map.svg.append('svg:path')
                            .attr('class', 'datamaps-arc')
                            .style('stroke-linecap', 'round')
                            .style('stroke', "rgba(" + arcColor.r + "," + arcColor.g + "," + arcColor.b + "," + Math.max(0.7, Math.random()) + ")")
                            .style('fill', 'none')
                            .style('stroke-width', $scope.arcThickness)
                            .attr('d', function(datum) {
                                var midXY = [(originXY[0] + destXY[0]) / 2, (originXY[1] + destXY[1]) / 2];
                                var sameCountry = (destXY[0] == originXY[0] && destXY[1] == originXY[1]);
                                var x = originXY[0],
                                    y = originXY[1],
                                    tx = destXY[0],
                                    ty = destXY[1],
                                    mx = midXY[0],
                                    my = midXY[1];
                                if (sameCountry) {
                                    var widthConst = 10;
                                    var heightConst = 10;
                                    var str = "M{0},{1}T{2},{3}T{4},{5}T{6},{7}T{8},{9}";
                                    return str.format(x, y, x + widthConst, y - heightConst, x, y - (2 * heightConst), x - widthConst, y - heightConst, x, y);
                                }
                                var str = "M{0},{1}S{2},{3},{4},{5}";
                                return str.format(x, y, mx + (50 * arcSharpness), Math.abs(my - (50 * arcSharpness)), tx, ty);
                            })
                            .transition()
                            .delay(100)
                            .style('fill', function() {
                                var length = this.getTotalLength();
                                this.style.strokeDasharray = length + ' ' + length;
                                this.style.strokeDashoffset = length;
                                var my = this;

                                if (isMSIE()) {
                                    var id = requestAnimationFrame(draw);

                                    function draw() {
                                        my.style.strokeDashoffset = parseInt(my.style.strokeDashoffset.replace('px', '')) - 10;
                                        if (parseInt(my.style.strokeDashoffset.replace('px', '')) < 10) {
                                            cancelAnimationFrame(id);
                                            arcHit(d3.select(my));
                                            squarePin.transition().duration($scope.fadeOutSpeed / 2)
                                                .style('opacity', 0)
                                                .remove();
                                            return;
                                        }
                                        id = requestAnimationFrame(draw);
                                    }
                                } else {
                                    this.getBoundingClientRect();
                                    this.style.transition = this.style.WebkitTransition = 'stroke-dashoffset ' + $scope.animationSpeed + 'ms ease-out';
                                    this.style.strokeDashoffset = '0';
                                    setTimeout(function() {
                                        arcHit(d3.select(my));
                                        squarePin.transition().duration($scope.fadeOutSpeed / 2)
                                            .style('opacity', 0)
                                            .remove();

                                    }, $scope.animationSpeed);
                                }

                                return 'none';
                            });

                        function arcHit(arc) {
                            arc
                                .transition()
                                .duration($scope.fadeOutSpeed / 2)
                                .style('opacity', 0)
                                .remove();

                            map.svg.append("svg:image")
                                .attr('x', destXY[0] - 11)
                                .attr('y', destXY[1] - 16)
                                .attr('width', 20)
                                .attr('height', 24)
                                .attr("xlink:href", "https://sc1.checkpoint.com/threatcloud/images/live-map/RoundedPin.png").transition()
                                .duration($scope.fadeOutSpeed / 2)
                                .style('opacity', 0)
                                .remove();

                            showCountryName(targetName,targetState, destXY[0] - 11, destXY[1] - 16, $scope.fadeOutSpeed / 2);

                            setTimeout(function() {

                                var img;

                                var images = [{
                                    src: "https://sc1.checkpoint.com/threatcloud/images/live-map/tinyExplosion.png",
                                    width: 16,
                                    height: 16
                                }, {
                                    src: "https://sc1.checkpoint.com/threatcloud/images/live-map/smallExplosion.png",
                                    width: 46,
                                    height: 45
                                }, {
                                    src: "https://sc1.checkpoint.com/threatcloud/images/live-map/bigExplosion.png",
                                    width: 57,
                                    height: 57
                                }, {
                                    src: "https://sc1.checkpoint.com/threatcloud/images/live-map/xLargeExplosion.png",
                                    width: 75,
                                    height: 75
                                }];
                                var img = images[Math.floor(Math.random() * (images.length - 1))];

                                map.svg.append("svg:image")
                                    .attr('x', destXY[0] - (img.width / 2))
                                    .attr('y', destXY[1] - (img.height / 2))
                                    .style('opacity', 0.5)
                                    .attr('width', img.width)
                                    .attr('height', img.height)
                                    .attr("xlink:href", img.src).transition()
                                    .duration($scope.fadeOutSpeed / 2)
                                    .style('opacity', 0)
                                    .remove();
                            }, 0);
                        }
                    }
                }

                function isMSIE() {
                    //if MSIE9 - we go to normal code execution, animation will not show.
                    return (window.ActiveXObject || "ActiveXObject" in window) && !window.isIE9;
                }

                function getCentroid(id) {
                    var result = null;
                    map.svg.selectAll(".datamaps-subunit")
                        .each(function(d) {
                            if (d.id == id) {
                                result = map.path.centroid(d.geometry);
                                if (id.toUpperCase() == "USA") {
                                    result[0] += 40;
                                    result[1] += 20;
                                }

                                if (id.toUpperCase() == "CAN") {
                                    result[1] += 20;
                                }


                                if (id.toUpperCase() == "NOR") {
                                    result[1] += 50;
                                }
                            }
                        });
                    return result;
                }
                function latLngToXY (lat, lng){     
                	return map.projection([lng, lat]);
                }

                function showCountryName(country,state, x, y, fadeOut, delay) {
                	
                	if(country === 'USA'){
                    	if (state !== undefined && state!== ""){
                    		country = state + "," + country;
                    	}
                    }
                	
                    if (!countryNamesDisplayed[country]) {
                        countryNamesDisplayed[country] = true;
                        var elem = map.svg.append("text")
                            .attr('x', x + 15)
                            .attr('y', y)
                            .attr('width', 20)
                            .attr('height', 24).text(country).attr("fill", "#fff").attr("font-size", "0.9em")
                            .transition();
                        if (delay) {
                            elem.delay(delay);
                        }
                        elem.transition().duration(fadeOut)
                            .style('opacity', 0)
                            .remove().each("end", function() {
                                countryNamesDisplayed[country] = null;
                            });
                    }
                }
            }
        };
    }]);
})(angular)
