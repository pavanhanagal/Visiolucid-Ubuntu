﻿(function(){
    var websocket;
    websocket = new WebSocket('ws:localhost:8089');

    websocket.onopen = function (e) {
        alert("You have have successfully connected to the server");

        var msgJSON = JSON.stringify(
		{"vectors":
		[
		{
            "sourcecity":"","destinationlongitude":-79.5197,"destinationlatitude":8.9936,"sourcestate":"","destinationstate":"8","type":"","sourcelongitude":37.6166,"sourcecountry":"RU","destinationcountry":"PA","sourcelatitude":55.75,"attackname":"Malware.yqxdi","destinationcity":"Panama City"
        },
		{
			"sourcecity":"","destinationlongitude":106.8294,"destinationlatitude":-6.1744,"sourcestate":"","destinationstate":"JK","type":"","sourcelongitude":9,"sourcecountry":"DE","destinationcountry":"ID","sourcelatitude":51,"attackname":"Malware.yqxdi","destinationcity":"Jakarta"
		},
		{
			"sourcecity":"Redmond","destinationlongitude":77,"destinationlatitude":20,"sourcestate":"WA","destinationstate":"","type":"","sourcelongitude":-122.1206,"sourcecountry":"US","destinationcountry":"IN","sourcelatitude":47.6801,	"attackname":"Trojan-Banker.Win32.Bancos.N",	"destinationcity":""
		},
		{
			"sourcecity":"","destinationlongitude":28.9647,"destinationlatitude":41.0186,"sourcestate":"","destinationstate":"34","type":"","sourcelongitude":9,"sourcecountry":"DE","destinationcountry":"TR","sourcelatitude":51,"attackname":"Operator.Bedep.hi","destinationcity":"Istanbul"
		}
		]});


        websocket.send(msgJSON);		
    }

})();
