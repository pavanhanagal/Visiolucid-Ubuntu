'use strict';



angular.module('threatPortal').config(["$provide", function($provide) {
        return $provide.decorator("$http", ["$delegate", function($delegate) {
            var get = $delegate.get;
            var cacheBustVersion = Math.random().toString(24).slice(2);
            $delegate.get = function(url, config) {

                // Check is to avoid breaking AngularUI ui-bootstrap-tpls.js: "template/accordion/accordion-group.html"
                if (url.indexOf('../partials/') !== -1) {
                    // Append ?v=[cacheBustVersion] to url
                    // /partials/smth.html?v=gasergsd
                    url += (url.indexOf("?") === -1 ? "?" : "&");
                    url += "v=" + cacheBustVersion;
                }
                return get(url, config);
            };
            return $delegate;
        }]);
    }]).config(['$compileProvider', function($compileProvider) {
        $compileProvider.debugInfoEnabled(false);
    }])
    .run(['$location', '$rootScope', '$stateParams', '$interval', '$state', function($location, $rootScope, $stateParams, $interval, $state) {
    	
    	$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            var query = $stateParams.query;
            var id = $stateParams.id;
            var t = '';
            if (query != null){
            	t = query;
            }
            else if (id != null){
            	t = id;
            }
            $rootScope.title = t + toState.title;
        });
    	
        $rootScope.BodyClass = {
            value: ""
        };
        
        $rootScope.getCurrentStateData = function() {

            return $state.current.data;
        };

        $rootScope.searchSubmitted = function() {
            $state.go("features.search", {
                query: this.query
            });
        };
    }]).constant("consts", {
        "checkForAttacks": "200"
    });




angular.module('liveMapApp').config(["$provide", function($provide) {
    return $provide.decorator("$http", ["$delegate", function($delegate) {
        var get = $delegate.get;
        var cacheBustVersion = Math.random().toString(24).slice(2);
        $delegate.get = function(url, config) {

            // Check is to avoid breaking AngularUI ui-bootstrap-tpls.js: "template/accordion/accordion-group.html"
            if (url.indexOf('../partials/') !== -1) {
                // Append ?v=[cacheBustVersion] to url
                // /partials/smth.html?v=gasergsd
                url += (url.indexOf("?") === -1 ? "?" : "&");
                url += "v=" + cacheBustVersion;
            }
            return get(url, config);
        };
        return $delegate;
    }]);
}]).config(['$compileProvider', function($compileProvider) {
    $compileProvider.debugInfoEnabled(false);
}])
.run(['$location', '$rootScope', '$interval', function($location, $rootScope, $interval) {
	
    $rootScope.BodyClass = {
        value: ""
    };
    

}]).constant("consts", {
    "checkForAttacks": "200"
});
