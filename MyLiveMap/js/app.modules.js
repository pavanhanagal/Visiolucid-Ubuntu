angular.module('threatPortal', ['ngSanitize', 'googlechart', 'ui.bootstrap', 'placeholderShim', 'threatPortal.filters',
    'threatPortal.services', 'threatPortal.directives', 'threatPortal.controllers', 'ui.router', 'angular.atmosphere', 'angles', 'ngDraggable'
]);
angular.module('threatPortal.controllers', []);
angular.module('threatPortal.directives', []);
angular.module('threatPortal.filters', []);
angular.module('threatPortal.services', []);




angular.module('liveMapApp', ['ngRoute', 'ngResource', 'ngSanitize', 'googlechart', 'ui.bootstrap', 'placeholderShim', 'threatPortal.filters',
	'threatPortal.services', 'threatPortal.directives', 'threatPortal.controllers', 'angular.atmosphere', 'angles', 'ngDraggable'
]);