(function(angular) {
    var app = angular.module('threatPortal.directives');

    app.directive('countryDrillDown', ['mapEventsService', '$window', '$timeout', 'serverService', 'CountryCodeService', '$q', function(mapEventsService, $window, $timeout, serverService, CountryCodeService, $q) {

        // Runs during compile
        return {
            // name: '',
            // priority: 1,
            // terminal: true,
            // scope: {
            // },
            // controller: function($scope, $element, $attrs, $transclude) {},
            // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
            restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
            // template: '',
            templateUrl: 'partials/live-map/directives/countryDrillDown.html',
            replace: true,
            // transclude: true,
            // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
            link: function($scope, iElm, iAttrs, controller) {
                $scope.me = iElm;
                $scope.isVisible = false;
                var ClickPosition = {
                    X: 0,
                    Y: 0
                };

                //if dragging happened, position doesn't change.
                var hasDragged = false;

                $scope.$on('draggable:end', function() {
                    hasDragged = true;
                });

                $scope.changeTimeSpanFilter = function(newVal) {
                    if (newVal != $scope.timeSpanFilter) {
                        loadData($scope.countryId, newVal).then(function() {
                            $scope.timeSpanFilter = newVal;
                            drawPopup(ClickPosition.X, ClickPosition.Y);
                        });
                    }
                };

                $scope.timeSpanFilter = 'Last Week';
                $scope.chartDataIndex = 1;
                
                function drawPopup(mouseX, mouseY) {
                    if (hasDragged) {
                        return;
                    }
                    $timeout(function() {
                        var topMargin = 104;
                        var margin = 50;
                        var my = $($scope.me);
                        var myWidth = my.width();
                        var myHeight = my.height();
                        var left = mouseX + margin,
                            top = mouseY - (myHeight / 2);
                        var windowWidth = $($window).width();
                        var windowHeight = $($window).height();

                        if (left < 0)
                            left = margin;
                        if (left + myWidth >= windowWidth)
                            left = windowWidth - myWidth - margin;
                        if (top < 0)
                            top = topMargin;
                        if ((top + myHeight + topMargin) >= windowHeight)
                            top = windowHeight - myHeight - 100;

                        if ((left + myWidth) > windowWidth || (top + myHeight) > windowHeight || top < 0 || left < 0) {
                            $scope.isVisible = false;
                        //    console.log("cannot display popup - screen too small");
                            $scope.timeSpanFilter = 'Last Week';
                            return;
                        }
                        $scope.position = {
                            left: left,
                            top: top
                        };
                        
                        $scope.isVisible = true;
                    }, 0);
                    
                  
                }

                mapEventsService.onCountryClicked(function(countryData) {
                    if (countryData.mouseX && countryData.mouseY) {
                        //happens when user clicks on map
                        ClickPosition.X = countryData.mouseX;
                        ClickPosition.Y = countryData.mouseY;

                    } else {
                        //happens when user clicks on top attacking/target countries.
                        ClickPosition.X = 350;
                        ClickPosition.Y = 400;
                    }

                    loadData(countryData.countryId, $scope.timeSpanFilter).then(function() {
                        drawPopup(ClickPosition.X, ClickPosition.Y);
                    });
                });

                function loadData(countryId, timeSpanFilter, mouseX, mouseY)
		{
                    var deferred = $q.defer();
			
			//Calling external php file using AJAX function call for loading JSON data for statistics of the respective countries that clicked from list. 
			  
			 $.ajax({

			  url: "webserver/drilldownserver.php",
			  type: "get", //send it through get method
			  data:{country:countryId},
			  success: function(response)
				 {
			    		//alert(response);
		       
					var getDrillDownData = atmosphere.util.parseJSON(response);


				        $scope.malwareTypes = getDrillDownData.infectingMalwareTypes ? getDrillDownData.infectingMalwareTypes.sort(function(x, y) {
				            return x.name === "Others";
				        }) : null;
				        $scope.mostAttackingCountry = getDrillDownData.mostAttackingCountry ? CountryCodeService.getByA2Code(getDrillDownData.mostAttackingCountry).name : null;

				        $scope.countryId = countryId;
				        $scope.countryName = CountryCodeService.getByA2Code(countryId).name;
				        $scope.chartData = [];
				        $scope.chartDataAvarage = [];
				        for (var i = 0; i < 4; i++) {
				            var clone = angular.copy(defaultChartData);
				            var arr = getDrillDownData.infectionRateChart ? getDrillDownData.infectionRateChart.slice(7 * i, 7 * (i + 1)) : [];
				            clone.datasets[0].data = _.map(arr, function(x) {
				                return x.count;
				            });
				            clone.labels = _.map(arr, function(x) {
				                switch (x.name) {
				                    case '1':
				                        return "S";
				                    case '2':
				                        return "M";
				                    case '3':
				                        return "T";
				                    case '4':
				                        return "W";
				                    case '5':
				                        return "T";
				                    case '6':
				                        return "F";
				                    case '7':
				                        return "S";
				                    default:
				                        return "";
				                }
				            });
				            $scope.chartData[i] = clone.datasets[0].data.length ? clone : null;
				            if ($scope.chartData[i] != null){
				            	$scope.chartDataAvarage[i] = calculateAverage($scope.chartData[i].datasets[0].data);
				            }
				        };

		                	deferred.resolve();
			  },
			  error: function(xhr) {
			    //Do Something to handle error
			  }

			});
 			return deferred.promise;
                }

                var defaultChartData = {
                    labels: ["S", "M", "T", "W", "T", "F", "S"],
                    datasets: [{
                        fillColor: "rgba(151,187,205,0)",
                        strokeColor: "#e95784",
                        pointColor: "rgba(151,187,205,0)",
                        pointStrokeColor: "rgba(151,187,205,0)"
                    }],
                };

                $scope.closeClicked = function() {
                    $scope.isVisible = false;
                    hasDragged = false;
                    $scope.malwareTypes = [];
                    $scope.position = {
                        left: -100,
                        top: -100
                    };
                };
                
                function calculateAverage (MyData) {
                    var sum = 0;
                    var length = MyData.length;
                    for (var i = 0; i < length; i++) {
                        sum += parseFloat(MyData[i], 10); //don't forget to add the base 
                    };
                    var avg = sum / MyData.length;
                    
                    if (avg < 0.01){
                    	avg = avg.toFixed(3);
                    }
                    else{
                    	avg =  avg.toFixed(2);
                    }
                    
                    if (avg == 0){
                    	avg =  "<0.01";
                    }
                    return avg;
                };

                $scope.myChartOptions = {

                    bezierCurve: false,
                    datasetStrokeWidth: 4,
                    // Boolean - Whether to animate the chart
                    animation: true,

                    // Number - Number of animation steps
                    animationSteps: 60,

                    // String - Animation easing effect
                    animationEasing: "easeOutQuart",
                    showTooltips: false,
                    scaleLineColor: "rgba(99,35,61,1)",
                    scaleGridLineColor: "rgba(99,35,61,1)",
                    scaleFontColor: "#fff",
                    scaleLineWidth: 2,
                    scaleGridLineWidth: 2

                };
            }
        };
    }]);
})(angular)
