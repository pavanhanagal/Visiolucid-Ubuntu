(function(angular) {
    var app = angular.module('threatPortal.directives');


    app.directive('inlineAnimation', [function() {
        return {
            restrict: 'A',
            link: function($scope, iElm, iAttrs, controller) {
                iAttrs.$observe('inlineAnimation', function(val) {
                    eval('var obj=' + val);
                    $(iElm).animate(obj.properties, obj.duration, obj.easing);
                });

            }
        };
    }]);
})(angular);
