(function(angular) {
    /* Services */
    'use strict';

    angular.module('threatPortal.services')
        .service("ServerServices", ['$http', function($http) {
            return {
                Statistics: function() {
                    var getStatisticsUrl = getUrl("statistics");
                    var getTickerUrl = getUrl("ticker");
                    return {
                        GetStatistics: function(familyFilter, activityFilter, chartType, dateFilter) {
                            return $http({
                                method: 'GET',
                                url: getStatisticsUrl,
                                params: {
                                    'familyFilter': familyFilter,
                                    'activityFilter': activityFilter,
                                    'chartType': chartType,
                                    'dateFilter': dateFilter

                                }
                            });
                        },
                        GetTicker: function() {
                            return $http({
                                method: 'GET',
                                url: getTickerUrl
                            });
                        }
                    }
                }(),
                Home: {
                    GetStartup: function() {
                        var startupUrl = getUrl("startup");
                        return $http({
                            method: 'GET',
                            url: startupUrl
                        });
                    }
                }
            }
        }]);

    var baseUrl = 'rest/threatPortal/';

    function getUrl(relative) {
        return baseUrl + relative;
    }

})(angular);
