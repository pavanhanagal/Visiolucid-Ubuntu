angular.module('threatPortal')
    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider.state('default', {
            url: '/',
            templateUrl: 'partials/home/index.html',
            title: 'Home Page | Threat Cloud',
            controller: 'StartupCtrl',
            data: {
                bodyClass: 'portal-bg'
            }
        });
        $stateProvider.state('features', {
            abstract: true,
            templateUrl: 'partials/features/index.html',
            data: {
                bodyClass: 'portal-inner-bg'
            }
        });
        $stateProvider.state('features.search', {
            url: '/search/:query',
            templateUrl: 'partials/features/search.html',
            controller: 'SearchCtrl',
            title: ' | Threat Cloud'
        });

        $stateProvider.state('features.malwareDetails', {
            url: '/malwareDetails/:id',
            templateUrl: 'partials/features/malwareDetails.html',
            title: ' | Threat Cloud',
            controller: 'MalwareDetailsCtrl'
        });


        $stateProvider.state('features.advisoryDetails', {
            url: '/advisoryDetails/:id',
            templateUrl: 'partials/features/advisoryDetails.html',
            title: ' | Threat Cloud',
            controller: 'AdvisoryDetailsCtrl'
        });

        // $stateProvider.state('features.statistics', {
        //     url: '/statistics',
        //     templateUrl: 'partials/features/statistics.html',
        //     controller: 'StatisticsCtrl',
        //     title: 'Statistics | Threat Cloud'
        // });
        $stateProvider.state('features.blogs', {
            url: '/blogs',
            templateUrl: 'partials/features/blogs.html',
            controller: 'BlogsCtrl',
            title: 'Blogs | Threat Cloud'
        });
        $stateProvider.state('map', {
            url: '/map',
            templateUrl: 'partials/live-map/index.html',
            controller: 'MapCtrl',
            title: 'Cybersecurity Threat Intelligence Map | Check Point Software'
        });
        $urlRouterProvider.otherwise('/');
        
//        $locationProvider.html5Mode(true);
    }]);



angular.module('liveMapApp')
.config(['$routeProvider', '$locationProvider',
   function ($routeProvider, $locationProvider) {
	  $routeProvider
	    .when('/', {
	        templateUrl: 'partials/live-map/index.html',
	        controller: 'MapCtrl',
	    })
	    .otherwise({
	      redirectTo: '/'
	    });
	  $locationProvider.html5Mode(true);
	}
]);
