(function(angular) {
    var app = angular.module('threatPortal.directives');

    app.directive('mobileStats', function() {

        // Runs during compile
        return {
            // name: '',
            // priority: 1,
            // terminal: true,
            scope: {
                topAttackers: '=',
                topTargets: '=',
                todayAttacksCount: '=',
                yesterdayAttacksCount: '='
            },
            // controller: function($scope, $element, $attrs, $transclude) {},
            // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
            restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
            // template: '',
            templateUrl: 'partials/live-map/directives/mobileStats.html',
            replace: true,
            // transclude: true,
            // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
            link: function($scope, iElm, iAttrs, controller) {}
        };
    });
})(angular)
