'use strict';

angular.module('threatPortal.directives')
    .directive('selection', function() {
        return {
            restrict: 'C',
            link: function(scope, element) {
                element.bind("click", function(e) {
                    element.parent().find("a").removeClass("selected");
                    element.addClass("selected");
                });
            }
        };
    })

.directive('collapsefilter', function() {
    return {
        restrict: "AC",
        transclude: true,
        replace: true,
        scope: {
            title: "@"
        },
        controller: function($scope, $element) {
            $scope.opened = true,
                $scope.toggle = function() {
                    $scope.opened = !$scope.opened;
                };
        },
        template: '<div class="collapseFilter">\
              <header ng-click="toggle()" onclick="event.stopPropagation();">\
                <h4>{{title}}</h4>\
              </header>\
                <section ng-transclude ng-class="{opened: opened}" onclick="event.stopPropagation();"></section>\
            </div>'
    };
})

.directive('malware', function() {
    return {
        restrict: "A",
        transclude: true,
        replace: true,
        scope: {
            doc: "=malware",
        },
        templateUrl:"partials/directives/malware.html"
    };
})

.directive('advisories', function() {
    return {
        restrict: "A",
        transclude: true,
        replace: true,
        scope: {
            doc: "=advisories"
        },
        templateUrl: "partials/directives/advisory.html"
    };
})

.directive('vbar', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: "partials/directives/vbar.html",
        scope: {
            url: '@',
            caption: '@',
            value: '@'
        }
    };
})

.directive('tabsettp', function() {
        return {
            restrict: 'E',
            replace: true,
            transclude: true,
            controller: function($scope) {
                $scope.templateUrl = '';
                var tabs = $scope.tabs = [];
                var controller = this;
                var selected = false;

                this.selectTab = function(tab) {
                    angular.forEach(tabs, function(tab) {
                        tab.selected = false;
                    });
                    tab.selected = true;
                };

                this.setTabTemplate = function(templateUrl) {
                    $scope.templateUrl = templateUrl;
                };

                this.addTab = function(tab) {
                    if (tab.content != '0' && !selected) {
                        controller.selectTab(tab);
                        selected = true;
                    }
                    tabs.push(tab);
                };
            },
            template: '<div class="row-fluid">' +
                '<div class="row-fluid">' +
                '<div class="nav nav-tabs" ng-transclude></div>' +
                '</div>' +
                '<div class="row-fluid">' +
                '<ng-include src="templateUrl"></ng-include>' +
                '</div>' +
                '</div>'
        };
    })
    .directive('tabtp', function() {
        return {
            restrict: 'E',
            replace: true,
            require: '^tabsettp',
            scope: {
                title: '@',
                templateUrl: '@',
                content: '@',
                icon: '@'
            },
            link: function(scope, element, attrs, tabsetController) {
                tabsetController.addTab(scope);

                scope.select = function() {
                    tabsetController.selectTab(scope);
                };

                scope.$watch('selected', function() {
                    if (scope.selected) {
                        tabsetController.setTabTemplate(scope.templateUrl);
                    }
                });
            },
            template: '<li ng-class="{active: selected}">' +
            '<div ng-click="select()">' +
            '<img ng-src="{{ icon }}" class="search-results-icon" />' +
              '<a class="search1" ng-href="">{{ title }}</a>' +
          '</div>' +'</li>'
        };
    });