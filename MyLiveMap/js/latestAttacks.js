(function(angular) {
    var app = angular.module('threatPortal.directives');

    app.directive('latestAttacks', ['mapEventsService', 'CountryCodeService', '$timeout', function(mapEventsService, CountryCodeService, $timeout) {

        return {
            restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
            // template: '',
            templateUrl: 'partials/live-map/directives/latestAttacks.html',
            replace: true,
            // transclude: true,
            // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
            link: function($scope, iElm, iAttrs, controller) {
                $scope.maxSize = 5;
                $scope.latestAttacks = [];
                $scope.countryNames = d3.map();
                $scope.date = function() {
                    return new Date();
                }

                var c = Datamap.prototype.worldTopo.objects.world.geometries;
                var length = c.length;
                for (var i = 0 ; i < length; i++) {
                    $scope.countryNames.set(c[i].id, c[i].properties.name);
                }
                $scope.$on("attack", function(event, attack) {
                    var sourceCountryData = CountryCodeService.getByA2Code(attack.sourcecountry).name;
                    var destinationCountryData = CountryCodeService.getByA2Code(attack.destinationcountry).name;
                    
                    if(sourceCountryData === 'USA'){
                    	if (attack.sourcestate !== undefined && attack.sourcestate !== ""){
                    		sourceCountryData = attack.sourcestate + "," + sourceCountryData;
                    	}
                    }
                    if(destinationCountryData === 'USA'){
                    	if (attack.destinationstate !== undefined && attack.destinationstate !== ""){
                    		destinationCountryData = attack.destinationstate + "," + destinationCountryData;
                    	}
                    }

                    $scope.latestAttacks.unshift({
                        attack: attack.attack,
                        sourcecountry: sourceCountryData,
                        destinationcountry: destinationCountryData,
                        color: getArcColor(attack.type)
                    });
                    if ($scope.latestAttacks.length > $scope.maxSize) {
                        $timeout(function() {
                            $scope.latestAttacks.pop();
                        }, 250);
                    }
                });
            }
        };
    }]);
})(angular)
